use std::io::{self, Read};

fn main() {
    let mut inp = io::stdin();
    let mut buff = String::new();
    inp.read_to_string(&mut buff).expect("Input error");
    let expected = "´".to_owned();
    if expected == buff {
        println!("2018");
    } else {
        panic!("Program error");
    }
}
